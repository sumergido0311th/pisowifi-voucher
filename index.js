"use strict"
/*
* Plugin Name: Piso Wifi Voucher
* Author: Reigel Gallarde
* version: 1.0.1
* Description: Add Voucher support.
*/
console.log('Voucher running');
var busy = {};
pisowifi.events.on('submit-voucher', ( data, target ) => {
  if ( busy[data.code] ) {return;}
  busy[data.code] = true;
  db.voucher.findOne({ where:{ code: { [db.Sequelize.Op.eq]: data.code } } })
    .then( voucher => {
      var result = {
        result: 'error'
      }
      if ( voucher ) {
        var v = voucher.get();
        result = {
          result: 'success',
          time: v.time,
          status: v.status
        }
        if ( v.status == 0 ) {
          voucher.update({status: 1})
          pisowifi.events.emit('addTime', data.mac, v.time);
        } else {
          
        }
      }
      return result;
    })
    .then( result => {
      busy[data.code] = false;
      result.event = 'submit-voucher';
      pisowifi.portal.send( data.mac, result );
    })
})